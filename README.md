# Maze solver
repository offers a robust set of tools designed for maze generation and solving. Implemented in Python, the project aims to provide a versatile solution for handling various maze-related challenges.

## Using technologies
- Python 3.11
- Pillow
### Installation
```
git clone https://gitlab.com/asergeya/gitlab-ci-demo
pip install -r requirements.txt
```

## Generate maze and solve it. Check maze_solution.gif
```
python main.py
```

## Run tests
```
pytest tests.py
```
