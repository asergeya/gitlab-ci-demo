from lib.maze import MazeSolver
from lib.gen_maze import generate_maze


matrix = generate_maze(25, 25)
maze = MazeSolver()
maze.solve(matrix, True)
