import json
from maze import MazeSolver
from gen_maze import generate_maze

def generate_test_cases(solver_instance, num_cases=10):
    test_cases = []
    for index in range(num_cases):
        print(f"Generate maze {index}")
        matrix = generate_maze(50, 50)
        result = solver_instance.solve(matrix)
        test_case = {'maze': matrix, 'result': result}
        test_cases.append(test_case)

    with open("maze_tests.json", "w", encoding='utf-8') as f:
        json.dump(test_cases, f, indent=4)

if __name__ == "__main__":
    solver = MazeSolver()
    generate_test_cases(solver)
