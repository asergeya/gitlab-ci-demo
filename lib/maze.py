from PIL import Image, ImageDraw


class MazeSolver:
    def __init__(self):
        self.maze = None
        self.visited = None
        self.zoom = 20
        self.start_point = None
        self.end_point = None

    @staticmethod
    def initialize_visited_matrix(maze):
        return [[0 for _ in row] for row in maze]

    def update_visited(self, step_number):
        changed = False
        for i, row in enumerate(self.visited):
            for j, val in enumerate(row):
                if val == step_number:
                    for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                        x, y = i + dx, j + dy
                        if (
                            0 <= x < len(self.visited)
                            and 0 <= y < len(row)
                            and self.visited[x][y] == 0
                            and self.maze[x][y] == 0
                        ):
                            self.visited[x][y] = step_number + 1
                            changed = True
        return changed

    def backtrack_path(self):
        i, j = self.end_point
        step_number = self.visited[i][j]
        path = [(i, j)]
        while step_number > 1:
            for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                x, y = i + dx, j + dy
                if (
                    0 <= x < len(self.visited)
                    and 0 <= y < len(self.visited[0])
                    and self.visited[x][y] == step_number - 1
                ):
                    i, j = x, y
                    path.append((i, j))
                    step_number -= 1
                    break
        return path[::-1]

    def draw_maze(self, path=None):
        image = Image.new(
            'RGB', (self.zoom * len(self.maze[0]), self.zoom * len(self.maze)),
            (255, 255, 255)
        )
        draw = ImageDraw.Draw(image)
        for i, row in enumerate(self.maze):
            for j, cell in enumerate(row):
                self.draw_cell(draw, i, j, cell, self.visited[i][j],
                               self.start_point, self.end_point, path)
        return image

    @staticmethod
    def draw_cell(draw, i, j, cell_value, visited_value, start, end, path):
        color = (0, 0, 0) if cell_value == 1 else (255, 255, 255)
        radius = 6 if (i, j) in [start, end] else 0
        draw.rectangle(
            (
                j * 20 + radius, i * 20 + radius,
                j * 20 + 20 - radius - 1, i * 20 + 20 - radius - 1
            ),
            fill=color
        )
        if visited_value > 0:
            draw.ellipse(
                (
                    j * 20 + radius, i * 20 + radius,
                    j * 20 + 20 - radius - 1, i * 20 + 20 - radius - 1
                ),
                fill=(255, 0, 0)
            )
        if path and (i, j) in path:
            draw.ellipse(
                (
                    j * 20 + radius, i * 20 + radius,
                    j * 20 + 20 - radius - 1, i * 20 + 20 - radius - 1
                ),
                fill=(0, 0, 255)
            )

    def solve(self, maze, save_gif=False):
        self.maze = maze
        self.visited = self.initialize_visited_matrix(maze)
        images = []
        self.end_point = self.find_end()
        for i, row in enumerate(maze):
            for j, cell in enumerate(row):
                if cell == 2:
                    self.start_point = (i, j)
                    self.visited[i][j] = 1

        step_number = 0
        while self.visited[self.end_point[0]][self.end_point[1]] == 0:
            step_number += 1
            changed = self.update_visited(step_number)
            if not changed:
                return []
            images.append(self.draw_maze())

        path = self.backtrack_path()
        if save_gif:
            for _ in range(10):
                images.append(self.draw_maze(path))

            images[0].save(
                'maze_solution.gif', save_all=True, append_images=images[1:],
                optimize=False, duration=10, loop=0
            )
        return path

    def find_end(self):
        for i in [0, len(self.maze) - 1]:
            for j, cell in enumerate(self.maze[i]):
                if cell == 0:
                    return i, j
        for i, row in enumerate(self.maze):
            for j in [0, len(row) - 1]:
                if row[j] == 0:
                    return i, j
        return None
