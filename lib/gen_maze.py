import random

def valid_directions_count(maze, row, col, directions):
    cnt = 0
    for dr, dc in directions:
        rr, cc = row + dr, col + dc
        if maze[rr][cc] == 0:
            cnt += 1
    return cnt

def generate_maze(rows, cols):
    maze = [[1 for _ in range(cols + 2)] for _ in range(rows + 2)]
    start_row, start_col = 1, 1
    maze[start_row][start_col] = 0
    stack = [(start_row, start_col)]
    directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]

    while stack:
        row, col = stack[-1]
        valid_directions = []

        for dr, dc in directions:
            new_row, new_col = row + dr, col + dc
            if 1 <= new_row < rows + 1 and 1 <= new_col < cols + 1 and maze[new_row][new_col] == 1:
                cnt = valid_directions_count(maze, new_row, new_col, directions)
                if cnt == 1:
                    valid_directions.append((dr, dc))

        if valid_directions:
            dr, dc = random.choice(valid_directions)
            new_row, new_col = row + dr, col + dc
            maze[new_row][new_col] = 0
            stack.append((new_row, new_col))
        else:
            stack.pop()

    add_exit_and_start(maze, rows, cols)
    return maze

def add_exit_and_start(maze, rows, cols):
    exit_side = random.choice(["top", "right", "bottom", "left"])
    if exit_side == "top":
        maze[0][random.randint(1, cols)] = 0
    elif exit_side == "right":
        maze[random.randint(1, rows)][cols + 1] = 0
    elif exit_side == "bottom":
        maze[rows + 1][random.randint(1, cols)] = 0
    elif exit_side == "left":
        maze[random.randint(1, rows)][0] = 0

    while True:
        start_row = random.randint(1, rows)
        start_col = random.randint(1, cols)
        if maze[start_row][start_col] == 0:
            maze[start_row][start_col] = 2
            break
