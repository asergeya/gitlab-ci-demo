import json
from lib.maze import MazeSolver


def test_maze():
    with open("lib/maze_tests.json", encoding='utf-8') as f:
        mazes = json.load(f)

    solver = MazeSolver()
    for maze in mazes:
        assert list(map(list, solver.solve(maze["maze"]))) == maze["result"]
